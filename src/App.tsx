import React, { useEffect, useState } from "react";
import "./App.css";

/* External Libs */

import { Route } from "react-router";

/* Pages */

import LandingPage from "./pages/landing/";
import NavBar from "./components/NavBar";
import LoginPage from "./pages/login";
import { RegisterPage } from "./pages/register";
import AppNavBar from "./components/AppNavBar";
import HomePage from "./pages/home";
import ProfilePage from "./pages/profile";
import { AddMotionPage } from "./pages/add";
import MyMotionsPage from "./pages/mymotions";

import { auth } from "./config/firebase";
import { Spin } from "antd";

interface IAppProps {
  history: any;
}

const App: React.FC<IAppProps> = (props) => {
  const [isLoading, setLoading] = useState(true);
  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        /* Destructuring */
        const { history } = props;
        if (
          [
            "/",
            "/auth/login",
            "/auth/",
            "/auth/register",
            "/app/",
            "/auth/login/",
            "/auth/register/",
          ].indexOf(
            // eslint-disable-next-line no-restricted-globals
            location.pathname
          ) > -1
        ) {
          history.push("/app/home");
        }
      } else {
        /* Destructuring */
        const { history } = props;
        // eslint-disable-next-line no-restricted-globals
        if (/app\/./.test(location.pathname)) {
          history.push("/");
        }
      }
      setLoading(false);
    });
  });

  return isLoading ? (
    <>
      <div
        style={{
          position: "relative",
          height: "100vh",
          background: "1c2431",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spin size="large"></Spin>
      </div>
    </>
  ) : (
    <>
      <Route path="/auth" component={NavBar} />
      <Route path="/app" component={AppNavBar} />
      <Route exact={true} path="/" component={LandingPage} />
      <Route exact={true} path="/auth/login" component={LoginPage} />
      <Route exact={true} path="/auth/register" component={RegisterPage} />
      <Route exact={true} path="/app/home" component={HomePage} />
      <Route exact={true} path="/app/profile" component={ProfilePage} />
      <Route exact={true} path="/app/add" component={AddMotionPage} />
      <Route
        exact={true}
        path="/app/profile/id/motions"
        component={MyMotionsPage}
      />
    </>
  );
};

export default App;
