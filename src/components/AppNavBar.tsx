import React, { useState } from "react";
import { Colors } from "../helpers/colors";
import { Link } from "react-router-dom";
import { HomeOutlined, UserOutlined, CommentOutlined } from "@ant-design/icons";
import { Menu, Row, Col } from "antd";
import { auth } from "../config/firebase";
import { Center } from "./Center";

export const AppNavBar = () => {
  const [current, setCurrent] = useState("");
  const { SubMenu } = Menu;

  const handleClick = (e: any) => {
    setCurrent(e.key);

    if (e.key === "exit") {
      handleSignOut();
    }
  };

  const handleSignOut = () => {
    auth.signOut();
  };

  return (
    <Row
      style={{ boxShadow: "0px 2px 30px -20px rgba(0,0,0,0.75)" }}
      align="middle"
    >
      <Col span={4}>
        <Center>
          <HomeOutlined />
          <Link to="/app/home" style={styles.routeLink}>
            VOX WEB
          </Link>
        </Center>
      </Col>
      <Col span={20}>
        <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
          <SubMenu
            title="Cuenta"
            icon={<UserOutlined />}
            style={{
              float: "right",
              color: Colors.primary,
              textDecoration: "none",
              fontSize: 16,
              fontWeight: 600,
              marginRight: "40px",
            }}
          >
            <Menu.Item key="profile">
              <Link to="/app/profile" style={styles.routeLink}>
                Perfil
              </Link>
            </Menu.Item>
            <Menu.Item key="mymotions">
              <Link to="/app/profile/id/motions" style={styles.routeLink}>
                Mis Mociones
              </Link>
            </Menu.Item>
            <Menu.Item key="exit">
              <Link to="/" style={styles.routeDangerLink}>
                Cerrar sesión
              </Link>
            </Menu.Item>
          </SubMenu>
          <Menu.Item
            key="home"
            icon={<CommentOutlined />}
            style={{ float: "right" }}
          >
            <Link to="/app/home" style={styles.routeLink}>
              Mociones
            </Link>
          </Menu.Item>
        </Menu>
      </Col>
    </Row>
  );
};

const styles = {
  navbar: {
    padding: "2vh 2vw",
    backgroundColor: "#FFF",
    boxShadow: "0px 2px 30px -20px rgba(0,0,0,0.75)",
  } as React.CSSProperties,
  homeLink: {
    color: Colors.primary,
    textDecoration: "none",
    fontSize: 16,
    fontWeight: "bold",
  } as React.CSSProperties,
  routeLink: {
    color: Colors.primary,
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 600,
    margin: "0 10px",
  } as React.CSSProperties,
  routeDangerLink: {
    color: Colors.danger,
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 600,
    margin: "0 10px",
  } as React.CSSProperties,
};

export default AppNavBar;
