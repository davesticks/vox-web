import React from "react";
import { Colors } from "../helpers/colors";
import { Link } from "react-router-dom";
import { HomeFilled } from "@ant-design/icons";

export const NavBar = () => {
  return (
    <div style={styles.navbar}>
      <Link to="/" style={styles.homeLink}>
        <HomeFilled style={{ color: Colors.primary, margin: "0 10px" }} />
        VOX WEB
      </Link>

      <div style={{ float: "right" }}>
        <Link to="/auth/login" style={styles.routeLink}>
          Ingreso
        </Link>
        <Link to="/auth/register" style={styles.routeLink}>
          Registro
        </Link>
      </div>
    </div>
  );
};

const styles = {
  navbar: {
    padding: "2vh 2vw",
    backgroundColor: "#FFF",
  } as React.CSSProperties,
  homeLink: {
    color: Colors.primary,
    textDecoration: "none",
    fontSize: 16,
    fontWeight: "bold",
  } as React.CSSProperties,
  routeLink: {
    color: Colors.primary,
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 600,
    margin: "0 10px",
  } as React.CSSProperties,
};

export default NavBar;
