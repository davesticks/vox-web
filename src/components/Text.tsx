import React from "react";

export const Text = (props: any) => {
  const { children } = props;
  return <p style={styles.text}> {children}</p>;
};

const styles = {
  text: {
    fontSize: 22,
    color: "#555",
  } as React.CSSProperties,
};
