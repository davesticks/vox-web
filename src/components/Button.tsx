import React from "react";
import { Colors } from "../helpers/colors";

const styles = {
  button: {
    backgroundColor: Colors.primary,
    padding: "12px 50px",
    border: 0,
    borderRadius: 100,
    color: "white",
    textTransform: "uppercase",
    fontWeight: 700,
    margin: "20px 0px",
  } as React.CSSProperties,
};

export const Button = (props: any) => {
  return <button {...props} style={styles.button} />;
};
