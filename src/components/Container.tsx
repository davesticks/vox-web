import React from "react";
import { Colors } from "../helpers/colors";

const styles = (bgColor: string) => ({
  container: {
    backgroundColor: bgColor ? bgColor : Colors.primary,
    height: "100vh",
    width: "100%",
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center",
  } as React.CSSProperties,
});

interface IContainer {
  bgColor?: string;
}

export default function Container(props: any) {
  const { children, bgColor } = props;
  return <div style={styles(bgColor).container}>{children}</div>;
}
