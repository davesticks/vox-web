import React from "react";

const styles = {
  input: {
    backgroundColor: "#fff",
    padding: "10px 15px",
    border: "1px solid #ddd",
    borderRadius: "100px",
    width: "calc(100% - 40px)",
    margin: "10px 0px",
  },
  header: {
    fontSize: "12px",
    color: "#777",
    textTransform: "uppercase",
    fontWeight: 700,
  } as React.CSSProperties,
};

interface IInputProps {
  placeholder?: string;
  label: string;
}

const Input: React.FC<IInputProps> = (props) => {
  const { label } = props;
  return (
    <div>
      <span style={styles.header}>{label}</span>
      <input {...props} style={styles.input} />
    </div>
  );
};

export default Input;
