import React from "react";

const styles = {
  card: {
    backgroundColor: "#fff",
    border: "1px solid #ddd",
    padding: "25px 50px",
  } as React.CSSProperties,
};

function Card(props: any) {
  const { children } = props;
  return <div style={styles.card}>{children}</div>;
}

export default Card;
