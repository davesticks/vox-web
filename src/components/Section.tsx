import React from "react";

const styles = {
  card: {
    padding: "10px 30px",
  } as React.CSSProperties,
};

function Section(props: any) {
  const { children } = props;
  return <div style={styles.card}>{children}</div>;
}

export default Section;
