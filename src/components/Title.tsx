import React from "react";
import { Colors } from "../helpers/colors";

interface ITitleProps {
  status?: string;
}

export const Title: React.FC<ITitleProps> = (props) => {
  const { children, status = "normal" } = props;
  return (
    <h2 {...props} style={styles(status).title}>
      {children}
    </h2>
  );
};

const checkStatus = (status: string) => {
  switch (status) {
    case "primary":
      return Colors.primary;
    case "danger":
      return Colors.danger;
    case "warning":
      return Colors.warning;
    case "success":
      return Colors.success;
    case "light":
      return "#FFF";
    case "normal":
      return "#555";
    default:
      return "#555";
  }
};

const styles = (status: string) => ({
  title: {
    color: checkStatus(status),
  } as React.CSSProperties,
});
