import {
  UserState,
  ActionTypes,
  FETCH_USER_PENDING,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR,
} from "../actions/types";

const initialState: UserState = {
  currentUser: {
    name: "",
    email: "",
    rut: "",
    score: 0,
    id: "",
    lastname: "",
    city: "",
  },
  pending: false,
  error: null,
};

export default (state = initialState, action: ActionTypes): UserState => {
  switch (action.type) {
    case FETCH_USER_PENDING:
      return {
        ...state,
        pending: true,
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        pending: false,
        currentUser: action.payload,
      };
    case FETCH_USER_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export const getUser = (state: UserState) => state.currentUser;
export const getUserPending = (state: UserState) => state.pending;
export const getUserError = (state: UserState) => state.error;
