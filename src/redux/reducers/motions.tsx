import {
  MotionState,
  ActionTypes,
  ADD_MOTION,
  FETCH_MOTIONS_PENDING,
  FETCH_MOTIONS_SUCCESS,
  FETCH_MOTIONS_ERROR,
} from "../actions/types";

const initialState: MotionState = {
  motions: [],
  pending: false,
  error: null,
};

export default (state = initialState, action: ActionTypes): MotionState => {
  switch (action.type) {
    case ADD_MOTION:
      return {
        motions: [...state.motions, action.payload],
      };
    case FETCH_MOTIONS_PENDING:
      return {
        ...state,
        pending: true,
      };
    case FETCH_MOTIONS_SUCCESS:
      return {
        ...state,
        pending: false,
        motions: action.payload,
      };
    case FETCH_MOTIONS_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export const getMotions = (state: MotionState) => state.motions;
export const getMotionsPending = (state: MotionState) => state.pending;
export const getMotionsError = (state: MotionState) => state.error;
