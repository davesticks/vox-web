export const ADD_MOTION = "ADD_MOTION";
export const REMOVE_MOTION = "REMOVE_MOTION";
export const FETCH_MOTIONS_PENDING = "FETCH_MOTIONS_PENDING";
export const FETCH_MOTIONS_SUCCESS = "FETCH_MOTIONS_SUCCESS";
export const FETCH_MOTIONS_ERROR = "FETCH_MOTIONS_ERROR";

/* MOTION */

export interface Motion {
  id?: string;
  title: string;
  description: string;
  category: string;
  likes: number;
  dislikes: number;
  voters: Array<any>;
  dateLimit: Date;
  address: string;
  author: string;
}

export interface MotionState {
  motions: Motion[];
  pending?: boolean;
  error?: any;
}

interface addMotionAction {
  type: typeof ADD_MOTION;
  payload: Motion;
}

interface removeMotionAction {
  type: typeof REMOVE_MOTION;
}

interface fetchMotionsPending {
  type: typeof FETCH_MOTIONS_PENDING;
}

interface fetchMotionsSuccess {
  type: typeof FETCH_MOTIONS_SUCCESS;
  payload: Motion[];
}

interface fetchMotionsError {
  type: typeof FETCH_MOTIONS_ERROR;
  error: any;
}

export const FETCH_USER_PENDING = "FETCH_USER_PENDING";
export const FETCH_USER_SUCCESS = "FETCH_USER_SUCCESS";
export const FETCH_USER_ERROR = "FETCH_USER_ERROR";

/* USER */

export interface User {
  id?: string;
  name: string;
  email: string;
  rut: string;
  score: number;
  lastname: string;
  city: string;
}

export interface UserState {
  currentUser: User;
  pending?: boolean;
  error?: any;
}

interface fetchUserPending {
  type: typeof FETCH_USER_PENDING;
}

interface fetchUserSuccess {
  type: typeof FETCH_USER_SUCCESS;
  payload: User;
}

interface fetchUserError {
  type: typeof FETCH_USER_ERROR;
  error: any;
}

export type ActionTypes =
  | addMotionAction
  | removeMotionAction
  | fetchMotionsPending
  | fetchMotionsSuccess
  | fetchMotionsError
  | fetchUserPending
  | fetchUserSuccess
  | fetchUserError;
