import {
  Motion,
  User,
  ADD_MOTION,
  REMOVE_MOTION,
  FETCH_MOTIONS_PENDING,
  FETCH_MOTIONS_SUCCESS,
  FETCH_MOTIONS_ERROR,
  FETCH_USER_PENDING,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR,
  ActionTypes,
} from "./types";

import { db, auth } from "../../config/firebase";

export function addMotion(newMotion: Motion): ActionTypes {
  return {
    type: ADD_MOTION,
    payload: newMotion,
  };
}

export function removeMotion(): ActionTypes {
  return {
    type: REMOVE_MOTION,
  };
}

export function fetchMotionsPending(): ActionTypes {
  return {
    type: FETCH_MOTIONS_PENDING,
  };
}

export function fetchMotionsSuccess(motions: Motion[]): ActionTypes {
  return {
    type: FETCH_MOTIONS_SUCCESS,
    payload: motions,
  };
}

export function fetchMotionsError(error: any): ActionTypes {
  return {
    type: FETCH_MOTIONS_ERROR,
    error: error,
  };
}

export function fetchMotions() {
  return async (dispatch: any) => {
    dispatch(fetchMotionsPending());
    var arrayData: Motion[] = [];
    await db
      .collection("Motions")
      .orderBy("dateLimit", "desc")
      .onSnapshot(
        (documentSnapshot) => {
          arrayData = documentSnapshot.docs.map((doc) => {
            const motion = {
              id: doc.id,
              title: doc.data().title,
              description: doc.data().description,
              category: doc.data().category,
              likes: doc.data().likes,
              dislikes: doc.data().dislikes,
              voters: doc.data().voters,
              address: doc.data().address,
              dateLimit: doc.data().dateLimit.toDate(),
              author: doc.data().author,
            };
            return motion as Motion;
          });
          dispatch(fetchMotionsSuccess(arrayData));
        },
        (error) => {
          dispatch(fetchMotionsError(error.message));
        }
      );
  };
}

/* USER ACTIONS */

export function fetchUserPending(): ActionTypes {
  return {
    type: FETCH_USER_PENDING,
  };
}

export function fetchUserSuccess(currentUser: User): ActionTypes {
  return {
    type: FETCH_USER_SUCCESS,
    payload: currentUser,
  };
}

export function fetchUserError(error: any): ActionTypes {
  return {
    type: FETCH_USER_ERROR,
    error: error,
  };
}

export function fetchUser() {
  return async (dispatch: any) => {
    dispatch(fetchUserPending());
    await db
      .collection("Users")
      .doc(auth.currentUser?.uid)
      .onSnapshot(
        (doc) => {
          const currentUserData = {
            id: auth.currentUser?.uid,
            email: doc?.data()?.email,
            name: doc?.data()?.name,
            lastname: doc?.data()?.lastname,
            city: doc?.data()?.city,
            rut: doc?.data()?.rut,
            score: doc?.data()?.score,
          } as User;
          dispatch(fetchUserSuccess(currentUserData));
        },
        (error) => {
          dispatch(fetchUserError(error));
        }
      );
  };
}
