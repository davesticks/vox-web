export const Colors = {
    primary: '#3366FF',
    success: '#2EC14B',
    info: '#4294F7',
    warning: '#FFC700',
    danger: '#FF4538',
    background: '#FFFFFF',
  };
  