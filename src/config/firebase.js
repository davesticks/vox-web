import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var firebaseConfig = {
    apiKey: "AIzaSyCemqrPDk-1lY9YtE2p9e1baLanYjCJj0Q",
    authDomain: "vox-votacion-ciudadana.firebaseapp.com",
    databaseURL: "https://vox-votacion-ciudadana.firebaseio.com",
    projectId: "vox-votacion-ciudadana",
    storageBucket: "vox-votacion-ciudadana.appspot.com",
    messagingSenderId: "882952940601",
    appId: "1:882952940601:web:5b94ac7b863b63c912b1e8",
    measurementId: "G-NE8SRLQ0FM"
};
// Initialize Firebase
const fb = firebase.initializeApp(firebaseConfig);
export const db = fb.firestore();
export const auth = fb.auth();