import React, { useState } from "react";
import {
  Row,
  Col,
  Form,
  Button,
  Checkbox,
  Input,
  Typography,
  Cascader,
  message,
} from "antd";
import { Center } from "../../components/Center";
import { ReactComponent as FormIcon } from "../../assets/images/form.svg";
import { styles } from "./styles";
import regions from "../../assets/json/communes.json";
import { auth, db } from "../../config/firebase";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export const RegisterPage = () => {
  /* Hooks */
  const [form] = Form.useForm();
  const [isLoading, setLoading] = useState(false);

  const { Item } = Form;
  const { Title } = Typography;

  const regionsOptions = regions.map((region) => ({
    value: region.numero,
    label: region.region,
    children: region.comunas.map((commune) => ({
      value: commune,
      label: commune,
    })),
  }));

  /* Methods */

  const onFinish = (values: any) => {
    console.log("Recieved values of form: ", values);
    handleRegister(values);
  };

  const handleRegister = (values: any) => {
    setLoading(true);
    auth
      .createUserWithEmailAndPassword(values.email, values.password)
      .then(() => {
        const user = auth.currentUser;
        if (user) {
          db.collection("Users")
            .doc(user.uid)
            .set({
              name: values.name,
              lastname: values.lastnames,
              rut: values.rut,
              score: 0,
              email: values.email,
              city: values.city[1] + ", Chile",
            })
            .then(() => {
              console.log("User created");
            })
            .catch((error) => {
              console.log(error);
            });
        }
        message.success("Has iniciado sesión correctamente");
        setLoading(false);
      })
      .catch((err) => {
        switch (err.code) {
          case "auth/invalid-email":
            message.error("El correo indicado es inválido");
            break;
          case "auth/email-already-in-use":
            message.error("Ese correo ya está en uso");
            break;
          case "auth/weak-password":
            message.error("La contraseña es muy débil");
            break;
          default:
            message.error(err.message);
        }
        setLoading(false);
      });
  };

  return (
    <>
      <br />
      <br />
      <Row align="middle" justify="center" style={styles.background}>
        <Col span={11} offset={1}>
          <Title level={3}>Formulario de Registro</Title>
          <br />
          <br />
          <Form
            {...layout}
            form={form}
            name="registerForm"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            scrollToFirstError
          >
            <Item
              label="Nombre"
              name="name"
              rules={[
                {
                  required: true,
                  message: "Por favor ingresa tu nombre",
                },
              ]}
            >
              <Input />
            </Item>
            <Item
              label="Apellidos"
              name="lastnames"
              rules={[
                {
                  required: true,
                  message: "Por favor ingresa tus apellidos",
                },
              ]}
            >
              <Input />
            </Item>
            <Item
              label="RUT Sin puntos ni guión"
              name="rut"
              rules={[
                {
                  required: true,
                  message: "Por favor ingresa tu número de cédula",
                },
                () => ({
                  validator(rule, value) {
                    if (value && value.match(/[-!@#$%^&*(),.?"/:{}|<>]/g)) {
                      return Promise.reject(
                        "Revisa el formato. No debe contener ni puntos ni guión"
                      );
                    }
                    return Promise.resolve();
                  },
                }),
              ]}
            >
              <Input />
            </Item>
            <Item
              label="Ciudad"
              name="city"
              rules={[
                {
                  type: "array",
                  required: true,
                  message: "Por favor selecciona tu ciudad",
                },
              ]}
            >
              <Cascader
                options={regionsOptions}
                placeholder="Selecciona una ciudad"
                allowClear={false}
              />
            </Item>
            <Item
              label="Correo electrónico"
              name="email"
              rules={[
                {
                  type: "email",
                  message: "El correo electrónico ingresado no es válido",
                },
                {
                  required: true,
                  message: "Por favor ingresa tu correo electrónico.",
                },
              ]}
            >
              <Input />
            </Item>
            <Item
              hasFeedback
              label="Contraseña"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Por favor ingresa tu contraseña",
                },
                () => ({
                  validator(rule, value) {
                    if (value && value.length < 8) {
                      return Promise.reject(
                        "La contraseña debe contener mínimo 8 carácteres"
                      );
                    }
                    return Promise.resolve();
                  },
                }),
              ]}
            >
              <Input.Password />
            </Item>
            <Item
              hasFeedback
              label="Repetir Contraseña"
              name="confirmPassword"
              dependencies={["password"]}
              rules={[
                {
                  required: true,
                  message: "Por favor ingresa tu contraseña",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject("Las contraseñas no coinciden");
                  },
                }),
              ]}
            >
              <Input.Password />
            </Item>

            <Item
              {...tailLayout}
              name="remember"
              valuePropName="checked"
              rules={[
                {
                  validator: (_, value) =>
                    value
                      ? Promise.resolve()
                      : Promise.reject(
                          "Debes aceptar los términos y condiciones"
                        ),
                },
              ]}
            >
              <Checkbox>Acepto los términos y condiciones</Checkbox>
            </Item>

            <Item {...tailLayout}>
              <Button loading={isLoading} type="primary" htmlType="submit">
                Registrarme
              </Button>
            </Item>
          </Form>
        </Col>
        <Col span={12}>
          <Center>
            <FormIcon width="70%" height="100%" />
          </Center>
          <br />
          <Center>
            <Title level={2}>Estás a un paso.</Title>
            <Title level={4}>Únete a Vox y participa como ciudadano</Title>
          </Center>
        </Col>
      </Row>
    </>
  );
};
