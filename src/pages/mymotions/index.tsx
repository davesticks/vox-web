import React, { useEffect, useState } from "react";
import { Row, Col, Typography, Divider, Table, Spin, Popconfirm } from "antd";
import { Center } from "../../components/Center";
import { Colors } from "../../helpers/colors";
import { ReactComponent as ListIcon } from "../../assets/images/list.svg";
import { ReactComponent as DislikeIcon } from "../../assets/images/dislikeStats.svg";
import { ReactComponent as LikeIcon } from "../../assets/images/likeStats.svg";
import { ReactComponent as Wave } from "../../assets/images/wave_3.svg";
import { db } from "../../config/firebase";
import { Motion, UserState } from "../../redux/actions/types";

/* Redux Imports */
import { connect } from "react-redux";
import { fetchUser } from "../../redux/actions";

interface IMyMotionsPageProps {
  fetchUserData: () => Promise<void>;
  currentUserData: UserState;
}

const MyMotionsPage: React.FC<IMyMotionsPageProps> = (props) => {
  /* Destructuring */
  const { Title } = Typography;
  const { currentUserData, fetchUserData } = props;
  const { currentUser } = currentUserData;
  /* Hooks */

  const [motions, setMotions] = useState<Motion[]>();

  useEffect(() => {
    fetchUserData();
    getUserMotions();
  }, []);

  const getUserMotions = async () => {
    await db
      .collection("Motions")
      .where("author", "==", currentUser.id)
      .orderBy("dateLimit", "desc")
      .onSnapshot((querySnapshot) => {
        var userMotions: Motion[] = [];
        querySnapshot.forEach((doc) => {
          const motion = {
            id: doc.id,
            title: doc.data().title,
            description: doc.data().description,
            category: doc.data().category,
            likes: doc.data().likes,
            dislikes: doc.data().dislikes,
            voters: doc.data().voters,
            address: doc.data().address,
            dateLimit: doc.data().dateLimit.toDate(),
            author: doc.data().author,
          } as Motion;
          userMotions.push(motion);
        });
        setMotions(userMotions);
      });
  };

  const getLikes = () => {
    var likes = 0;
    motions?.forEach((motion) => {
      likes = likes + motion.likes;
    });
    return likes;
  };

  const getDislikes = () => {
    var dislikes = 0;
    motions?.forEach((motion) => {
      dislikes = dislikes + motion.dislikes;
    });
    return dislikes;
  };

  const columns = [
    {
      title: "Título",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Categoría",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Dirección",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Likes",
      dataIndex: "likes",
      key: "likes",
    },
    {
      title: "Dislikes",
      dataIndex: "dislikes",
      key: "dislikes",
    },
    {
      title: "Operación",
      dataIndex: "operation",
      render: (text: any, record: Motion) =>
        motions ? (
          <Popconfirm
            title="¿Seguro que quieres eliminar esta moción?"
            onConfirm={() => {
              console.log(record.id);
              db.collection("Motions").doc(record.id).delete();
            }}
          >
            <a>Eliminar</a>
          </Popconfirm>
        ) : null,
    },
  ];

  /* COMPONENTS */

  const CustomDivider = () => {
    return (
      <Row>
        <Col span={12} offset={9}>
          <div
            style={{
              height: 2,
              width: "50%",
              backgroundColor: "#e7e7e7",
              margin: "40px 0px",
            }}
          ></div>
        </Col>
      </Row>
    );
  };

  /* RENDER */

  return (
    <>
      {/* Header */}
      <br />
      <br />
      <Divider>
        <Title level={2} style={{ color: Colors.primary }}>
          Mis estadísticas
        </Title>
      </Divider>
      <br />
      {/* TOTAL MOTIONS */}
      <Row align="middle">
        <Col span={8} offset={4}>
          <Center>
            <ListIcon width="30%" height="30%" />
          </Center>
        </Col>
        <Col span={12}>
          <Title level={4}>
            Has levantado{" "}
            {motions ? (
              motions?.length !== 1 ? (
                motions.length + " mociones"
              ) : (
                motions.length + " moción"
              )
            ) : (
              <Spin />
            )}
          </Title>
        </Col>
      </Row>
      <CustomDivider />
      {/* TOTAL LIKES */}
      <Row align="middle">
        <Col span={6} offset={6}>
          <Title level={4}>
            Has recibido {motions ? getLikes() : <Spin />} likes en total
          </Title>
        </Col>
        <Col span={8} pull={2}>
          <Center>
            <LikeIcon width="35%" height="100%" />
          </Center>
        </Col>
      </Row>
      <CustomDivider />
      {/* TOTAL DISLIKES */}
      <Row align="middle">
        <Col span={8} offset={4}>
          <Center>
            <DislikeIcon width="35%" height="30%" />
          </Center>
        </Col>
        <Col span={12}>
          <Title level={4}>
            Has recibido {motions ? getDislikes() : <Spin />} dislikes en total
          </Title>
        </Col>
      </Row>
      <Row>
        <Wave width="100%" height="50%" />
      </Row>
      {/* TABLE TITLE */}
      <Row style={{ backgroundColor: Colors.primary }}>
        <Col span={24}>
          <Center>
            <Title level={2} style={{ color: "#FFF" }}>
              Listado de Mociones
            </Title>
          </Center>
        </Col>
      </Row>
      {/* SEPARATOR */}
      <div style={{ backgroundColor: Colors.primary, height: 20 }} />
      <Row style={{ backgroundColor: Colors.primary }}>
        <Col span={20} offset={2}>
          <Center>
            <Table dataSource={motions} columns={columns} rowKey="id" />;
          </Center>
        </Col>
      </Row>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {
    currentUserData: state.user,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchUserData: () => dispatch(fetchUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyMotionsPage);
