import React, { useEffect, useState } from "react";

/* External Imports */

import {
  Row,
  Col,
  Typography,
  Input,
  Checkbox,
  Divider,
  Button,
  Drawer,
  Form,
  Select,
  Spin,
  message,
  Empty,
  DatePicker,
} from "antd";

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";

/* Local imports  */
import { Center } from "../../components/Center";
import { PlusOutlined } from "@ant-design/icons";
import { Colors } from "../../helpers/colors";
import { ReactComponent as AddContent } from "../../assets/images/addContent.svg";
import Motion from "../../containers/app/Motion";

/* Redux Imports */
import { connect } from "react-redux";
import { fetchMotions, fetchUser } from "../../redux/actions";
import {
  Motion as IMotion,
  MotionState,
  User,
  UserState,
} from "../../redux/actions/types";
import { slugify } from "../../helpers/strings";
import Featured from "../../containers/app/Featured";
import { auth, db } from "../../config/firebase";
import { CheckboxValueType } from "antd/lib/checkbox/Group";

interface IHomePageProps {
  currentUserData: UserState;
  motionsData: MotionState;
  fetchMotionsData: () => Promise<void>;
  fetchUserData: () => Promise<void>;
}

const HomePage: React.FC<IHomePageProps> = (props) => {
  /* Destructuring */
  const { Title, Text } = Typography;
  const { Search } = Input;
  const {
    fetchMotionsData,
    fetchUserData,
    motionsData,
    currentUserData,
  } = props;
  const { currentUser } = currentUserData;
  const { motions } = motionsData;
  /* Hooks */
  const [drawer, setDrawer] = useState(false);
  const [searchInput, setSearchInput] = useState("");
  const [featuredUsers, setFeaturedUsers] = useState<User[]>();
  const [checkedValues, setCheckedValues] = useState<CheckboxValueType[]>([]);

  useEffect(() => {
    fetchMotionsData();
    fetchUserData();
    getFeaturedUsers();
  }, []);

  const options = [
    { label: "Deportes", value: "sports" },
    { label: "Cultura", value: "culture" },
    { label: "Educación", value: "education" },
    { label: "Calidad de Vida", value: "lifeQuality" },
    { label: "Medio ambiente", value: "environment" },
  ];

  /* Methods */

  const showDrawer = () => {
    setDrawer(true);
  };
  const onClose = () => {
    setDrawer(false);
  };

  const getFeaturedUsers = async () => {
    await db
      .collection("Users")
      .orderBy("score", "desc")
      .limit(10)
      .onSnapshot((querySnapshot) => {
        var featuredUsers: User[] = [];
        querySnapshot.forEach((doc) => {
          featuredUsers.push(doc.data() as User);
        });
        setFeaturedUsers(featuredUsers);
      });
  };

  const filteredMotions = () => {
    /* Lets apply searchbar filter first */
    const searchBarMotions = motions.filter((motion: IMotion) => {
      return slugify(motion.title)
        .toLowerCase()
        .includes(slugify(searchInput).toLowerCase());
    });
    /* Now it applies the category filter */
    const categoryMotions = searchBarMotions.filter((motion: IMotion) => {
      /* If at least one checkbox is checked */
      if (checkedValues.length > 0) {
        /* compare checked values with this motion category */
        return checkedValues.includes(motion.category);
      }
      /* if not */
      return motion;
    });
    return categoryMotions;
  };

  const renderFeaturedUsers = (featuredUsers: User[]) => {
    return featuredUsers.map((user: User, index) => {
      return (
        <Featured
          name={user.name}
          lastname={user.lastname}
          score={user.score}
          key={index}
        />
      );
    });
  };

  const renderMotions = (motions: IMotion[]) => {
    if (motions.length > 0) {
      return motions.map((motion: IMotion, index) => {
        return (
          <Motion
            id={motion.id}
            author={motion.author}
            title={motion.title}
            description={motion.description}
            category={motion.category}
            likes={motion.likes}
            dislikes={motion.dislikes}
            voters={motion.voters}
            dateLimit={motion.dateLimit}
            address={motion.address}
            key={index}
          />
        );
      });
    } else {
      return (
        <div style={{ marginTop: 50 }}>
          <Center>
            <Empty />
          </Center>
        </div>
      );
    }
  };

  const DrawerForm = () => {
    /* Today */
    var now = new Date();
    /* Add two weeks */
    now.setDate(now.getDate() + 14);

    /* Destructuring */
    const { Item } = Form;
    const { TextArea } = Input;
    const { Option } = Select;

    /* Hooks  */
    const [address, setAddress] = useState("");
    const [dateLimit, setDateLimit] = useState(now);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [location, setLocation] = useState<google.maps.LatLngLiteral>();

    const onFinish = (values: any) => {
      console.log("Recieved values of form: ", values);
      var motion = {
        title: values.motionTitle,
        address: address,
        author: auth?.currentUser?.uid,
        likes: 0,
        dislikes: 0,
        category: values.motionCategory,
        dateLimit: dateLimit,
        description: values.motionDescription,
        voters: [],
      } as IMotion;
      handleAdd(motion);
      /* handleRegister(values); */
    };

    const handleAdd = (motion: IMotion): void => {
      setDrawer(false);
      db.collection("Motions")
        .add(motion)
        .then(async () => {
          console.log("Moción añadida exitosamente");
          message.success("La moción ha sido añadida");
          db.collection("Users")
            .doc(currentUser.id)
            .update({ score: currentUser.score + 1 })
            .then(() => {
              console.log(
                "Se ha actualizado el puntaje del usuario " + currentUser.id
              );
            });
        })
        .catch((error) => {
          message.error(error);
        });
    };

    const handleChange = (address: any) => {
      setAddress(address);
    };

    const handleSelect = (address: any) => {
      setAddress(address);
      geocodeByAddress(address)
        .then((results) => getLatLng(results[0]))
        .then((latLng) => {
          console.log("Success", latLng);
          setLocation(latLng);
        })
        .catch((error) => console.error("Error", error));
    };

    return (
      <Drawer
        placement="right"
        closable={true}
        onClose={onClose}
        visible={drawer}
        width="35vw"
        footer={
          <div
            style={{
              textAlign: "right",
            }}
          >
            <Button type="primary" htmlType="submit" form="addMotionForm">
              Agregar moción
            </Button>
          </div>
        }
      >
        <Title level={4}>Nueva moción</Title>
        <div style={{ height: 5, backgroundColor: Colors.primary }} />
        <br />
        <Center>
          <AddContent width="40%" height="40%" />
        </Center>
        <br />
        <Text>
          ¿Te sientes inspirado? Rellena todos los campos del siguiente
          formulario y levanta una moción haciendo click en "Agregar Moción"
        </Text>
        <div style={{ height: 30 }}></div>
        <Form
          hideRequiredMark
          layout="vertical"
          name="addMotionForm"
          onFinish={onFinish}
        >
          <Row>
            <Col span={24}>
              <Item label="Fecha límite" name="dateLimit">
                <Input disabled placeholder={dateLimit.toLocaleDateString()} />
              </Item>
            </Col>
          </Row>

          <Row>
            <Col span={24}>
              <Item
                label="Título"
                name="motionTitle"
                rules={[
                  {
                    required: true,
                    message: "Por favor ingresa un título para tu moción",
                  },
                ]}
              >
                <Input />
              </Item>
            </Col>
          </Row>

          <Item
            label="Ubicación"
            name="motionLocation"
            rules={[
              {
                required: true,
                message: "Por favor ingresa la ubicación de tu moción",
              },
            ]}
          >
            <PlacesAutocomplete
              value={address}
              onChange={handleChange}
              onSelect={handleSelect}
              searchOptions={{
                componentRestrictions: {
                  country: "cl",
                },
              }}
            >
              {({
                getInputProps,
                suggestions,
                getSuggestionItemProps,
                loading,
              }) => (
                <div style={{ width: "100%" }}>
                  <Input {...getInputProps({})} value={address} />
                  <div>
                    {loading && (
                      <div style={{ textAlign: "center", marginTop: 20 }}>
                        <Spin />
                      </div>
                    )}
                    {suggestions.map((suggestion, index) => {
                      const className = suggestion.active
                        ? "suggestion-item--active"
                        : "suggestion-item";
                      // inline style for demonstration purpose
                      const style = suggestion.active
                        ? { backgroundColor: "#fafafa", cursor: "pointer" }
                        : { backgroundColor: "#ffffff", cursor: "pointer" };
                      return (
                        <div
                          {...getSuggestionItemProps(suggestion, {
                            className,
                            style,
                          })}
                          key={index}
                        >
                          <span>{suggestion.description}</span>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}
            </PlacesAutocomplete>
          </Item>
          <Item
            label="Categoría"
            name="motionCategory"
            rules={[
              {
                required: true,
                message: "Por favor selecciona una categoría para tu moción",
              },
            ]}
          >
            <Select placeholder="Selecciona una categoría">
              <Option value="education">Educacion</Option>
              <Option value="culture">Cultura</Option>
              <Option value="qualityLife">Calidad de Vida</Option>
              <Option value="sports">Deportes</Option>
              <Option value="environment">Medio Ambiente</Option>
              <Option value="mobility">Movilidad</Option>
            </Select>
          </Item>
          <Item
            label="Descripción"
            name="motionDescription"
            rules={[
              {
                required: true,
                message:
                  "Por favor brinda un texto que logre describir la moción propuesta",
              },
            ]}
          >
            <TextArea maxLength={300} />
          </Item>
        </Form>
      </Drawer>
    );
  };

  return (
    <>
      <DrawerForm />
      {/* HEADER */}
      <br />
      <br />
      <Row justify="space-around">
        {/* LEFT SIDE */}
        <Col span={15} offset={1}>
          {/* SEARCH BAR */}
          <Row align="middle">
            <Col span={18} offset={2}>
              <Search
                size="large"
                placeholder="Buscar moción"
                onSearch={(value) => setSearchInput(value)}
                enterButton
                style={{ width: "90%" }}
              />
            </Col>
            <Col span={4} pull={1}>
              <Button ghost type="primary" onClick={showDrawer}>
                <PlusOutlined />
                Crear
              </Button>
            </Col>
          </Row>
          <br />
          <br />
          {/* CATEGORIES */}
          <Row>
            <Col span={22} style={{ marginLeft: "100px" }}>
              <Title level={3} style={{ color: Colors.primary }}>
                Categorías
              </Title>
            </Col>
          </Row>
          <Row>
            <Col span={22} style={{ marginLeft: "100px" }}>
              <Checkbox.Group
                options={options}
                defaultValue={["none"]}
                onChange={(checkedValues) => setCheckedValues(checkedValues)}
              />
            </Col>
          </Row>
          {/* MOTIONS */}
          <Row align="middle">
            <Col span={22} offset={2}>
              {motionsData.pending ? (
                <div
                  style={{
                    position: "relative",
                    height: "100vh",
                    background: "1c2431",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Spin />
                </div>
              ) : (
                renderMotions(filteredMotions())
              )}
            </Col>
          </Row>
        </Col>
        {/* RIGHT SIDE */}
        <Col span={6}>
          <Center>
            <Title style={{ color: Colors.primary }} level={4}>
              Usuarios destacados
            </Title>
          </Center>
          <Divider />
          {/* TOP USERS */}
          {featuredUsers ? (
            renderFeaturedUsers(featuredUsers)
          ) : (
            <Center>
              <Spin />
            </Center>
          )}

          <br />
        </Col>
      </Row>
      <br />
      <br />
    </>
  );
};

/* Connect with Redux Store */

const mapStateToProps = (state: any) => {
  return {
    motionsData: state.motions,
    currentUserData: state.user,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchMotionsData: () => dispatch(fetchMotions()),
    fetchUserData: () => dispatch(fetchUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
