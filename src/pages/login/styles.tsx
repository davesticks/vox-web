export const styles = {
  polygonsBackground: {
    backgroundImage: `url(${require("../../assets/images/cubos.svg")})`,
    backgroundRepeat: "no-repeat",
    width: "100%",
    height: "100vh",
    position: "absolute",
    zIndex: -1,
  } as React.CSSProperties,
};
