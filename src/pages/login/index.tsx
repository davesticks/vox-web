import React, { useState } from "react";

/* TODO:
    - Validation on fields using firebase */

/* Local imports */
import { ReactComponent as Login } from "../../assets/images/login.svg";
import { Center } from "../../components/Center";

/* External imports */
import {
  Row,
  Col,
  Form,
  Button,
  Checkbox,
  Input,
  Typography,
  message,
} from "antd";
import { auth } from "../../config/firebase";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export const LoginPage = () => {
  /* Destructuring props */
  const { Item } = Form;
  const { Title } = Typography;
  /* Hooks */
  const [isLoading, setLoading] = useState(false);

  const handleLogin = (values: any) => {
    setLoading(true);
    auth
      .signInWithEmailAndPassword(values.email, values.password)
      .then(() => {
        message.success("Has iniciado sesión correctamente");
        setLoading(false);
      })
      .catch((err) => {
        switch (err.code) {
          case "auth/invalid-email":
            message.error("El correo ingresado no es válido");
            break;
          case "auth/user-disable":
            message.error(
              "El usuario ingresado se encuentra desactivado. Contacta a un adminsitrador"
            );
            break;
          case "auth/user-not-found":
            message.error("El usuario especificado no ha sido encontrado");
            break;
          case "auth/wrong-password":
            message.error("La contraseña ingresada es incorrecta");
            break;
        }
        setLoading(false);
      });
  };

  const onFinish = (values: any) => {
    /* if all fields are filled we login */
    handleLogin(values);
    console.log("Values recieved from form: ", values);
  };

  return (
    <>
      <br />
      <br />
      <br />
      <Row align="middle" justify="center">
        <Col span={14}>
          <br />
          <Center>
            <Login width="60%" height="100%" />
          </Center>
          <br />
          <br />
          <Center>
            <Title level={3} style={{ color: "#4f4d40" }}>
              ¿Cuántas mociones nos esperan hoy?
            </Title>
          </Center>
        </Col>
        <Col span={10} pull={2}>
          <Center>
            <Title>Inicio de sesión</Title>
          </Center>
          <br />
          <br />
          <Form
            {...layout}
            name="loginForm"
            initialValues={{ remember: true }}
            onFinish={onFinish}
          >
            <Item
              label="Correo electrónico"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Por favor ingresa tu correo electrónico",
                },
              ]}
            >
              <Input onChange={(e) => console.log(e)} />
            </Item>
            <Item
              label="Contraseña"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Por favor ingresa tu contraseña",
                },
              ]}
            >
              <Input.Password />
            </Item>

            <Item {...tailLayout} name="remember" valuePropName="checked">
              <Checkbox>Recuérdame</Checkbox>
            </Item>

            <Item {...tailLayout}>
              <Button type="primary" htmlType="submit" loading={isLoading}>
                {/* <Link to="/app/home">Ingresar</Link> */}
                Ingresar
              </Button>
            </Item>
          </Form>
        </Col>
      </Row>
    </>
  );
};

export default LoginPage;
