import { Colors } from "../../helpers/colors";

export const styles = {
  header: {
    backgroundColor: "rgba(0,0,0,0)",
    backgroundImage: `url(${require("../../assets/images/wave.svg")})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  } as React.CSSProperties,
  title: {
    color: "#4f4d40",
  } as React.CSSProperties,
  light: {
    color: "#FFF",
  } as React.CSSProperties,
  text: {
    fontSize: 16,
  } as React.CSSProperties,
  features: {
    backgroundColor: Colors.primary,
  },
  polygonsBackground: {
    backgroundImage: `url(${require("../../assets/images/cubos.svg")})`,
    backgroundRepeat: "no-repeat",
    width: "100%",
    height: "100vh",
    position: "absolute",
    zIndex: -1,
  } as React.CSSProperties,
};
