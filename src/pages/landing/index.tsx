import React from "react";

/* External Imports */
import { Row, Col, Typography, Button, Divider, Card } from "antd";
import { Link } from "react-router-dom";

/* Local Imports */
import { ReactComponent as Web } from "../../assets/images/web.svg";
import { ReactComponent as LikeDislike } from "../../assets/images/likedislike.svg";
import { ReactComponent as Create } from "../../assets/images/create.svg";
import { ReactComponent as Participate } from "../../assets/images/participate.svg";
import { Center } from "../../components/Center";
import { styles } from "./styles";
import { Colors } from "../../helpers/colors";
import NavBar from "../../components/NavBar";

const LandingPage = () => {
  /* Destructuring */
  const { Title, Text } = Typography;

  return (
    <>
      <NavBar />
      <div style={styles.polygonsBackground}></div>

      {/* HEADER SECTION */}

      <Row align="middle" style={styles.header}>
        {/* Left Side */}
        <Col span={12} offset={2}>
          <Title style={styles.title}>Bienvenido a Vox</Title>
          <Title style={styles.title} level={4}>
            Un sistema inteligente diseñado para que puedas participar en las
            decisiones de tu comuna. Levanta mociones, vota y sé parte del
            cambio.
          </Title>
          {/* Buttons Section */}
          <br />
          <br />
          <Row justify="end">
            <Col flex={1}>
              <Button type="primary" shape="round" size="large">
                <Link to="/auth/register">Crear una cuenta</Link>
              </Button>
            </Col>
            <Col flex={5}>
              <Button type="default" shape="round" size="large">
                <Link to="/auth/login">Ingresar con mi cuenta</Link>
              </Button>
            </Col>
          </Row>
        </Col>
        {/* Right Side */}
        <Col span={10}>
          <Center>
            <Web width="80%" height={600} />
          </Center>
        </Col>
      </Row>

      {/* FEATURES SECTION */}

      <Row align="middle" style={styles.features} justify="center">
        <Col span={24}>
          <Center>
            <Divider style={{ color: "white" }}>
              <Title style={styles.light}>Características</Title>
            </Divider>
          </Center>
        </Col>
      </Row>
      {/* Separator */}
      <div style={{ backgroundColor: Colors.primary, height: 50 }} />
      <Row
        align="middle"
        justify="center"
        style={{ backgroundColor: Colors.primary }}
      >
        {/* Vote */}
        <Col span={7}>
          <Card style={{ borderRadius: 20, margin: "10px", height: 500 }}>
            <Center>
              <LikeDislike width="50%" height={200} />
            </Center>
            <Center>
              <Title level={3} style={styles.title}>
                Vota
              </Title>
            </Center>
            <Text style={styles.text}>
              Apenas ingreses podrás ver el listado completo de todas las
              mociones levantadas por otros ciudadanos. Basta con ingresar al
              detalle de estas a través de un click para emitir un voto a través
              de un amigable sistema de likes y dislikes. ¿Qué estás esperando?
              ¡Registrate!
            </Text>
          </Card>
        </Col>

        {/* Create */}
        <Col span={7}>
          <Card style={{ borderRadius: 20, margin: "10px", height: 500 }}>
            <Center>
              <Create width="50%" height={200} />
            </Center>
            <Center>
              <Title level={3} style={styles.title}>
                Crea
              </Title>
            </Center>
            <Text style={styles.text}>
              Una vez hayas creado una cuenta, podrás levantar de forma
              inmediata una moción. Esta se mostrará en un listado junto a otras
              para que cualquier persona que viva en tu misma ciudad pueda darle
              like o dislike. ¿Tienes una gran idea? ¡Ingresa ahora mismo!
            </Text>
          </Card>
        </Col>

        {/* Participate */}
        <Col span={7}>
          <Card style={{ borderRadius: 20, margin: "10px", height: 500 }}>
            <Center>
              <Participate width="50%" height={200} />
            </Center>
            <Center>
              <Title level={3} style={styles.title}>
                Participa
              </Title>
            </Center>
            <Text style={styles.text}>
              Mientras más interactúes con la aplicación, ya sea votando o
              levantando mociones, mayor será tu puntaje de participación. Quien
              sabe, ¿tal vez pronto puedas canjear esos puntos por servicios o
              cupones validos para tu ciudad?
            </Text>
          </Card>
        </Col>
      </Row>

      <div style={{ backgroundColor: Colors.primary, height: 50 }} />
    </>
  );
};

export default LandingPage;
