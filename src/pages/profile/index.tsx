import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Avatar,
  Typography,
  Divider,
  Button,
  Spin,
  Empty,
  message,
  Popconfirm,
} from "antd";
import { Center } from "../../components/Center";
import { UserOutlined } from "@ant-design/icons";
import { Colors } from "../../helpers/colors";
import { Link } from "react-router-dom";
import { db, auth } from "../../config/firebase";

/* Redux imports */
import { connect } from "react-redux";
import { fetchUser } from "../../redux/actions";
import { Motion as IMotion } from "../../redux/actions/types";
import Motion from "../../containers/app/Motion";

const { Title, Text } = Typography;

const ProfilePage = (props: any) => {
  /* Destructuring */
  const { currentUserData, fetchUserData } = props;
  const { currentUser, pending } = currentUserData;
  const [lastMotion, setLastMotion] = useState<IMotion>();

  useEffect(() => {
    fetchUserData();
    getUserMotions();
  }, []);

  const getUserMotions = async () => {
    await db
      .collection("Motions")
      .where("author", "==", auth.currentUser?.uid)
      .orderBy("dateLimit", "desc")
      .onSnapshot((querySnapshot) => {
        var userMotions: IMotion[] = [];
        querySnapshot.forEach((doc) => {
          const motion = {
            id: doc.id,
            title: doc.data().title,
            description: doc.data().description,
            category: doc.data().category,
            likes: doc.data().likes,
            dislikes: doc.data().dislikes,
            voters: doc.data().voters,
            address: doc.data().address,
            dateLimit: doc.data().dateLimit.toDate(),
            author: doc.data().author,
          } as IMotion;
          userMotions.push(motion);
        });
        setLastMotion(userMotions[0]);
      });
  };

  return (
    <>
      <br />
      <br />
      <Row align="middle">
        <Col span={3} offset={9}>
          <Center>
            <Avatar
              size={100}
              icon={<UserOutlined />}
              style={{ backgroundColor: Colors.success }}
            />
          </Center>
        </Col>
        <Col span={12}>
          <Row align="middle">
            <Title level={4}>
              {pending ? (
                <Spin />
              ) : (
                currentUser.name + " " + currentUser.lastname
              )}
            </Title>
          </Row>
          <Row align="middle">
            <Title level={5} style={{ color: "gray" }}>
              {pending ? <Spin /> : currentUser.city}
            </Title>
          </Row>
        </Col>
      </Row>
      <br />
      <br />
      <br />
      <Row justify="space-between">
        {/* PERSONAL DATA */}
        <Col span={11}>
          <Divider style={{ borderColor: "red", borderWidth: 10 }}>
            <Title level={4} style={{ color: Colors.primary }}>
              Datos Personales
            </Title>
          </Divider>
          {/* NAME & LASTNAME */}
          <Row>
            {/* NAME */}
            <Col span={10} offset={2}>
              <Row>
                <Col span={24}>
                  <Center>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                      Nombre
                    </Text>
                  </Center>
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={24}>
                  <Center>
                    <Text>{pending ? <Spin /> : currentUser.name}</Text>
                  </Center>
                </Col>
              </Row>
            </Col>
            {/* LASTNAME */}
            <Col span={10}>
              <Row>
                <Col span={24}>
                  <Center>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                      Apellido
                    </Text>
                  </Center>
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={24}>
                  <Center>
                    <Text>{pending ? <Spin /> : currentUser.lastname}</Text>
                  </Center>
                </Col>
              </Row>
            </Col>
          </Row>
          {/* RUT & SCORE */}
          <br />
          <Row>
            {/* RUT */}
            <Col span={10} offset={2}>
              <Row>
                <Col span={24}>
                  <Center>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                      Cédula o RUT
                    </Text>
                  </Center>
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={24}>
                  <Center>
                    <Text copyable>{pending ? <Spin /> : currentUser.rut}</Text>
                  </Center>
                </Col>
              </Row>
            </Col>
            {/* SCORE */}
            <Col span={10}>
              <Row>
                <Col span={24}>
                  <Center>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                      Puntaje Ciudadano
                    </Text>
                  </Center>
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={24}>
                  <Center>
                    <Text>{pending ? <Spin /> : currentUser.score}</Text>
                  </Center>
                </Col>
              </Row>
            </Col>
          </Row>
          {/* EMAIL & CHANGE EMAIL */}
          <br />
          <Row>
            {/* EMAIL */}
            <Col span={10} offset={2}>
              <Row>
                <Col span={24}>
                  <Center>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                      E-mail
                    </Text>
                  </Center>
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={24}>
                  <Center>
                    <Text>{pending ? <Spin /> : currentUser.email}</Text>
                  </Center>
                </Col>
              </Row>
            </Col>
            {/* BUTTON */}
            <Col span={10}>
              <Row>
                <Col span={24}>
                  <Center>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                      <br />
                    </Text>
                  </Center>
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={24}>
                  <Center>
                    <Button>Cambiar</Button>
                  </Center>
                </Col>
              </Row>
            </Col>
          </Row>
          {/* PASSWORD & CHANGE PASSWORD */}
          <br />
          <Row>
            {/* PASSWORD */}
            <Col span={10} offset={2}>
              <Row>
                <Col span={24}>
                  <Center>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                      Contraseña
                    </Text>
                  </Center>
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={24}>
                  <Center>
                    <Text>*********</Text>
                  </Center>
                </Col>
              </Row>
            </Col>
            {/* BUTTON */}
            <Col span={10}>
              <Row>
                <Col span={24}>
                  <Center>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                      <br />
                    </Text>
                  </Center>
                </Col>
              </Row>
              <br />
              <Row>
                <Col span={24}>
                  <Center>
                    <Popconfirm
                      title="¿Seguro que quieres restaurar tu contraseña?"
                      onConfirm={() => {
                        auth
                          .sendPasswordResetEmail(currentUser.email)
                          .then(() => message.success("Enviado con exito"))
                          .catch((error) => {
                            console.log(error);
                          });
                      }}
                    >
                      <Button>Cambiar</Button>
                    </Popconfirm>
                  </Center>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        {/* LAST MOTIONS */}
        <Col span={12} pull={1}>
          <Divider style={{ borderColor: "red", borderWidth: 10 }}>
            <Title level={4} style={{ color: Colors.primary }}>
              Última moción levantada
            </Title>
          </Divider>
          {!pending ? (
            lastMotion ? (
              <Motion
                id={lastMotion?.id}
                address={lastMotion.address}
                author={lastMotion.author}
                category={lastMotion.category}
                dateLimit={lastMotion.dateLimit}
                description={lastMotion.description}
                dislikes={lastMotion.dislikes}
                likes={lastMotion.likes}
                title={lastMotion.title}
                voters={lastMotion.voters}
              />
            ) : (
              <Center>
                <Empty />
              </Center>
            )
          ) : (
            <Center>
              <Spin />
            </Center>
          )}
          <br />
          <Center>
            <Button>
              <Link to="/app/profile/id/motions">Ver Todas </Link>
            </Button>
          </Center>
        </Col>
      </Row>
      <br />
      <br />
      <br />
    </>
  );
};

/* Connect with Redux Store */

const mapStateToProps = (state: any) => {
  return {
    currentUserData: state.user,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchUserData: () => dispatch(fetchUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
