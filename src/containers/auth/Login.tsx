import React from "react";

/* Components */

import Card from "../../components/Card";
import Container from "../../components/Container";
import Input from "../../components/Input";
import { Button } from "../../components/Button";
import { Title } from "../../components/Title";
import { Center } from "../../components/Center";
import { Link } from "react-router-dom";

/* SVG */

import { ReactComponent as Voting } from "../../assets/images/voting.svg";
import { Divider } from "antd";

export const Login = () => {
  return (
    <>
      <Container bgColor="#FFF">
        <div style={styles.imageSection}>
          <Voting style={styles.image} />
        </div>
        <Card>
          <Title>Inicia Sesión</Title>
          <Divider />
          <Input label="Correo Electrónico" />
          <Input label="Contraseña" />
          <Center>
            <Button>Entrar</Button>
          </Center>
          <Center>
            <Link to="/register">No tengo una cuenta</Link>
          </Center>
        </Card>
      </Container>
    </>
  );
};

const styles = {
  image: {
    width: "40vw",
    height: "40vh",
    margin: "20px",
  } as React.CSSProperties,
  imageSection: {
    width: "40vw",
  } as React.CSSProperties,
};

export default Login;
