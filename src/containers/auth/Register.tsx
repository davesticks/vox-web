import React from "react";

/* External Components */

import { Link } from "react-router-dom";
import { Divider } from "antd";

/* Components */

import Card from "../../components/Card";
import Container from "../../components/Container";
import Input from "../../components/Input";
import { Button } from "../../components/Button";
import { Title } from "../../components/Title";
import { Center } from "../../components/Center";

export const Register = () => {
  return (
    <>
      <Container>
        <Card>
          <Title>Formulario de Registro</Title>
          <Divider />
          <Input label="Nombre Completo" />
          <Input label="RUT (Sin puntos ni guión)" />
          <Input label="Ciudad" />
          <Input label="Correo Electrónico" />
          <Input label="Contraseña" />
          <Center>
            <Button>Entrar</Button>
          </Center>
          <Center>
            <Link to="/">Ya tengo una cuenta</Link>
          </Center>
        </Card>
      </Container>
    </>
  );
};
