import { UserOutlined } from "@ant-design/icons";
import { Avatar, Col, Row, Typography } from "antd";
import React from "react";
import { Center } from "../../components/Center";
import { Colors } from "../../helpers/colors";

interface IFeaturedProps {
  name: string;
  lastname: string;
  score: number;
}

const Featured: React.FC<IFeaturedProps> = (props) => {
  /* Destructuring */
  const { name, score, lastname } = props;
  const { Title } = Typography;
  return (
    <>
      <Row align="middle" style={{ margin: "10px 0px" }}>
        <Col span={3}>
          <Avatar
            size={50}
            icon={<UserOutlined />}
            style={{ backgroundColor: Colors.primary }}
          />
        </Col>
        <Col span={17}>
          <Center>
            <Title style={{ color: "gray", fontSize: 17 }}>
              {name + " " + lastname}{" "}
            </Title>
          </Center>
        </Col>
        <Col span={3}>
          <Center>
            <Title level={4}>{score}</Title>
          </Center>
        </Col>
      </Row>
    </>
  );
};

export default Featured;
