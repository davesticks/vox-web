import React, { useEffect, useState } from "react";

/* External imports */

import {
  Card,
  Row,
  Col,
  Badge,
  Divider,
  Button,
  Modal,
  Typography,
  Avatar,
  Space,
  Tag,
  message,
} from "antd";

import {
  UserOutlined,
  LikeFilled,
  DislikeFilled,
  CalendarOutlined,
  CompassOutlined,
} from "@ant-design/icons";

import { db } from "../../config/firebase";

/* Local imports */

import { Colors } from "../../helpers/colors";
import { Center } from "../../components/Center";
import { Motion as IMotion, UserState } from "../../redux/actions/types";

/* Redux imports */

import { connect } from "react-redux";
import { fetchUser } from "../../redux/actions";

interface IMotionReduxProps {
  currentUserData: UserState;
  fetchUserData: () => Promise<void>;
}

type IMotionProps = IMotion & IMotionReduxProps;

const Motion: React.FC<IMotionProps> = (props) => {
  /* Const Destructuring */
  const { Text, Title } = Typography;
  const {
    id,
    title,
    description,
    category,
    likes,
    dislikes,
    voters,
    dateLimit,
    address,
    author,
  } = props;

  const { currentUserData } = props;
  const { currentUser } = currentUserData;

  /* Hooks */
  const [visible, setVisible] = useState(false);
  const [authorName, setAuthorName] = useState("");

  useEffect(() => {
    db.collection("Users")
      .doc(author)
      .get()
      .then((doc) => {
        if (doc.exists) {
          setAuthorName(doc?.data()?.name + " " + doc?.data()?.lastname);
        } else {
          setAuthorName("Desconocido");
        }
      });
  }, [author]);

  /* Messages */

  const success = () => {
    message.success("Se ha emitido tu voto");
  };

  const error = () => {
    message.error("Ya has votado en esta moción");
  };

  /* Methods */

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (e: any) => {
    console.log(e);
    setVisible(false);
  };

  const handleCancel = (e: any) => {
    console.log(e);
    setVisible(false);
  };

  const hasVoted = () => {
    if (voters.includes(currentUser.id)) {
      return true;
    } else {
      return false;
    }
  };

  const likeMotion = () => {
    if (currentUser) {
      if (hasVoted()) {
        error();
      } else {
        db.collection("Motions")
          .doc(id)
          .update({
            likes: likes + 1,
            voters: [...voters, currentUser.id],
          })
          .then(() => {
            console.log("Motion " + id + " updated!");
            success();
            db.collection("Users")
              .doc(currentUser.id)
              .update({ score: currentUser.score + 1 })
              .then(() => {
                console.log(
                  "Se ha actualizado el puntaje para el usuario " +
                    currentUser.id
                );
              });
          });
      }
    }
  };

  const dislikeMotion = () => {
    if (currentUser) {
      if (hasVoted()) {
        error();
      } else {
        db.collection("Motions")
          .doc(id)
          .update({
            dislikes: dislikes + 1,
            voters: [...voters, currentUser.id],
          })
          .then(() => {
            console.log("Motion " + id + " updated!");
            success();
            db.collection("Users")
              .doc(currentUser.id)
              .update({ score: currentUser.score + 1 })
              .then(() => {
                console.log(
                  "Se ha actualizado el puntaje para el usuario " +
                    currentUser.id
                );
              });
          });
      }
    }
  };

  return (
    <>
      <Badge.Ribbon
        text={hasVoted() ? "Ya has votado" : ""}
        color={hasVoted() ? Colors.success : Colors.info}
        style={{ visibility: hasVoted() ? "visible" : "hidden" }}
      >
        <Card
          style={{
            width: "100%",
            marginTop: "20px",
            boxShadow: "2px 2px 28px -19px rgba(51,102,255,1)",
          }}
        >
          <Row>
            <Col span={24}>
              <Row>
                <Col span={24}>
                  <Space size={25}>
                    <Title level={5}>{title}</Title>
                  </Space>
                </Col>
              </Row>
              <div style={{ height: 5 }} />
              <Row>
                <Col span={24}>
                  <Space size={20}>
                    <Tag color={Colors.primary}>{category}</Tag>
                    <Space size={5}>
                      <CalendarOutlined />
                      <Text>
                        Vence {dateLimit.toLocaleString().split(" ")[0]}
                      </Text>
                    </Space>
                    <Space size={5}>
                      <CompassOutlined />
                      <Text>{address}</Text>
                    </Space>
                    <Space size={5}>
                      <Badge title="Hola" />
                    </Space>
                  </Space>
                </Col>
              </Row>
            </Col>
          </Row>
          <Divider />
          <p>{description}</p>
          <Divider />
          <Center>
            <Button type="primary" onClick={showModal}>
              Ver Detalles
            </Button>
            {/* MODAL */}
            <Modal
              visible={visible}
              onOk={handleOk}
              onCancel={handleCancel}
              width="50vw"
              footer={[
                <Button ghost key="okay" type="primary" onClick={handleOk}>
                  OK
                </Button>,
              ]}
            >
              <Row>
                <Col span={24}>
                  <Title level={4} style={{ color: Colors.primary }}>
                    {title}
                  </Title>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <div
                    style={{
                      width: "100%",
                      height: 5,
                      backgroundColor: Colors.primary,
                    }}
                  />
                </Col>
              </Row>
              <br />
              <Row align="middle">
                <Col span={5}>
                  <Center>
                    <Avatar
                      size={64}
                      icon={<UserOutlined />}
                      style={{ backgroundColor: Colors.success }}
                    />
                  </Center>
                </Col>
                <Col span={11}>
                  <Title style={{ color: "gray", fontSize: 18 }}>
                    {authorName}
                  </Title>
                </Col>
                <Col span={6}>
                  <Center>
                    <Title style={{ color: "gray", fontSize: 18 }}>
                      Vence {dateLimit.toLocaleString().split(" ")[0]}
                    </Title>
                  </Center>
                </Col>
              </Row>
              <Divider />
              <Row align="middle">
                <Col span={8}>
                  <div style={{ textAlign: "end" }}>
                    <Button
                      type="link"
                      shape="circle"
                      style={{ height: 100, width: 100 }}
                      icon={
                        <LikeFilled
                          style={{ color: Colors.primary, fontSize: 40 }}
                        />
                      }
                      onClick={likeMotion}
                    />
                  </div>
                </Col>
                <Col span={4}>
                  <Center>
                    <Title style={{ color: Colors.primary }} level={2}>
                      {hasVoted() ? likes : ""}
                    </Title>
                  </Center>
                </Col>
                <Col span={4}>
                  <Center>
                    <Title style={{ color: Colors.danger }} level={2}>
                      {hasVoted() ? dislikes : ""}
                    </Title>
                  </Center>
                </Col>
                <Col span={8}>
                  <div style={{ textAlign: "start" }}>
                    <Button
                      type="link"
                      danger
                      shape="circle"
                      style={{ height: 100, width: 100 }}
                      icon={
                        <DislikeFilled
                          style={{
                            color: Colors.danger,
                            fontSize: 40,
                          }}
                        />
                      }
                      onClick={dislikeMotion}
                    />
                  </div>
                </Col>
              </Row>
              <Divider />
              <Text style={{ fontSize: 16 }}>{description}</Text>
            </Modal>
          </Center>
        </Card>
      </Badge.Ribbon>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {
    currentUserData: state.user,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchUserData: () => dispatch(fetchUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Motion);
