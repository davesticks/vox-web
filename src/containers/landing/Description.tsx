import React, { useState } from "react";

/* Ant Design */

import { Steps, Divider } from "antd";

/* Components */

import Container from "../../components/Container";
import { Title } from "../../components/Title";
import { Center } from "../../components/Center";

/* SVG */

import { ReactComponent as Information } from "../../assets/images/information_tab.svg";

export const Description = () => {
  const [currentStep, setCurrentStep] = useState(0);
  const { Step } = Steps;

  /* Methods */

  const onChange = (current: number) => {
    console.log("onChange: ", current);
    setCurrentStep(current);
  };

  return (
    <Container bgColor="#eee">
      <div style={styles.textSection}>
        <Center>
          <Title>¿CÓMO FUNCIONA VOX?</Title>
        </Center>

        <Divider />

        <Steps current={currentStep} onChange={onChange} direction="vertical">
          <Step
            title="Registrate"
            description="Para acceder a todas las funcionalidades de Vox debes estar registrado en la plataforma. ¡Arriba puedes hacerlo!"
          />
          <Step
            title="Vota"
            description="Apenas ingreses a Vox tendrás a tu disposición un listado de mociones. ¿Qué esperas para entrar y apoyar una de estas?"
          />
          <Step
            title="Crea"
            description="Si ya tienes una cuenta seguro que lo primero que vas a querer hacer será levantar una moción. Esto es muy fácil, intuitivo y totalmente gratis!, ¿qué tal si lo pruebas?"
          />
        </Steps>
        <Divider />
      </div>
      <div style={styles.imageSection}>
        <Information style={styles.image} />
      </div>
    </Container>
  );
};

const styles = {
  imageSection: {
    width: "30vw",
  } as React.CSSProperties,
  image: {
    width: "30vw",
    height: "70vh",
  } as React.CSSProperties,
  textSection: {
    width: "40vw",
  },
};
